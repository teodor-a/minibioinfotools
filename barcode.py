#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###########################################################

Title: barcode.py
Date: Sun Oct 17 2021
Author: Teodor Alling (https://teodor-a.github.io)
   
Description:
    This was an assignment for the course 'Programming in Python' as part of
        the Bioinformatics Master Programme at Lund University 2021.
    The program will read a fastq file, trim away some predetermined barcodes
        from the sequences, and write the trimmed DNA and quality to a new
        fastq file. It writes sequences with no barcode to a separate output
        file.

List of functions:
    No user defined functions are used in the program.

List of 'non standard' modules:
    sys        useful functions for using arguments with your program
    re         handy regex functions
    pathlib    I use Path() functions for file and path related housekeepinge

Procedure:   
    1. Read DNA file line by line. If the line is a header, just print it.
    2. If the line is a DNA sequence with a barcode,remove the barcode string
        and print the remaining sequence to output fastq file.
    3. If the line contains no barcode, print the entry (sequences or
       sequence + quality value) to a different output fastq file.
    4. Print the related quality score sequence to the relevant output file.
        Separate it from the DNA sequence with a single line containing '+',
        as is standard for the fastq file format. If the DNA sequence was
        trimmed, also trim the quality score sequence in the same position.

Usage:
    python barcode.py input_filename.fastq output_file.txt [optional]
    i.e. python barcode.py [path to fastq file] [optional path to output file]
    
Known bugs:
    - not optimised to also be able to handle fasta as input
    - if there are several barcodes on the same DNA sequence, will only
        trim the first barcode.

########################################################### 
"""


#%%
#######################################
### Import modules ####################
#######################################

import sys
from pathlib import Path
import re


#%%
#######################################
### Arguments and housekeeping ########
#######################################

# Make sure that the input file exists:
if Path(sys.argv[1]).is_file():
    # Takes the argument strings provided and stores them in a variable each:
    infile_fasta = sys.argv[1]
    if len(sys.argv) > 2:
        outfile_txt = sys.argv[2]
    else:
        print('No output file specified by user, using default filename.')
        outfile_txt = 'barcode_output.fastq'
# If the input file is missing, print error:
else:
    print('\nCould not find input file. Please make sure you are using the \
script as desribed in the documentation.\n \
(python barcode.py [path to fastq file] [path to output file])')
    # After this, the program quits:
    print('Exiting. Please try again.\n')
    exit()




#%%
#######################################
### Read DNA file and remove barcodes #
#######################################

# Test it:
#infile_fasta = 'barcode.fastq'
#outfile_txt = 'out.fastq'
    
# Initialise three variables to store the data in as we work our way through
# the input file:
tempheader = ''
tempseq = ''
tempqual = ''

# Initialise three variables to keep track of which lines store header, DNA
# sequence, or quality sequence, respectively.
# The first header will appear on line index 0 of the input file:
nextheaderline = 0
# The first DNA sequence will appear on line index 1 of the input file:
nextseqline = 1
# The first quality sequence will appear on line index 3 of the input file:
nextqualline = 3 

# These are the barcodes we will work with, stored in a list which we will use
# later:
barcodes = ['TATCCTCT', 'GTAAGGAG', 'TCTCTCCG']

# Keep the input file open for reading, and both outut files (one for trimmed
# sequences and one for sequences with no barcode) open for writing:
with open(infile_fasta, 'r') as dnafile, \
open(outfile_txt, 'w') as outfastq, \
open('undetermined.fastq', 'w') as outfast2:
    # Iterate over the (enumerated) rows of the input fastq file:
    for linenumber, line in enumerate(dnafile):
        # First of all, strip the current line from newline chars:
        line = line.strip()
        # Check if the current line index matches the line index of the next
        # expected header line:
        if linenumber == nextheaderline:
            # If this is a header line, store it in a variable:
            tempheader = line
            # Then, add 4 to the 'nextheaderline' variable. The first header
            # will be on line index 0, the next on line index 4, and so on:
            nextheaderline += 4
        # Then, check if the current line index matches the line index of the
        # next expected DNA sequence line:
        if linenumber == nextseqline:
            # If this is a DNA sequence line, store it in a variable:
            tempseq = line.upper()
            # Then, add 4 to the 'nextseqline' variable. The first DNA sequence
            # will be on line index 1, the next on line index 5, and so on:
            nextseqline += 4
        # Then, check if the current line index matches the line index of the
        # next expected quality sequence line:
        if linenumber == nextqualline:
            # If this is a quality sequence line, store it in a variable:
            tempqual = line
            # Then, add 4 to the 'nextqualline' variable. The first quality
            # sequence will be on line index 3, the next on line index 7, and
            # so on:
            nextqualline += 4
            # Then, since this is the last line relating to this 'set' of lines
            # with information about a single DNA sequence, we are ready to
            # print the information stored in our temporary variables, to the
            # relevant output files. But first we must find and trim the
            # barcodes. First check if any of the barcodes occurs in the DNA
            # sequence:
            if re.search('TATCCTCT|GTAAGGAG|TCTCTCCG', tempseq) != None:
                # If any of the barcodes occur in the DNA sequence, iterate
                # over the list containing the three barcodes:
                for barcode in barcodes:
                    # For each barcode, search for it in the DNA sequence and
                    # store the match object in variable 'm':
                    m = re.search(barcode, tempseq)
                    # Check if m is empty:
                    if m != None:
                        # Assign the DNA sequence string to a new variable. But
                        # most importantly, assign the part of the DNA sequence
                        # from beginning up to where the match found above
                        # starts (as given with m.span()[0]), as well as the
                        # part from where the match found above ends (as given
                        # with m.span()[1]) to the end of the string. This is
                        # my way of cutting out the barcode.
                        newseq = tempseq[0:m.span()[0]] + tempseq[m.span()[1]:]
                        # Also trim the quality sequence in the same way:
                        newqual = tempqual[0:m.span()[0]] + tempqual[m.span()[1]:]
                        # If we have found a match, we should stop the for loop
                        # or else we will get errors since NoneType objects
                        # will be returned for the barcodes that are not found.
                        break
                # Now, we are ready to print our header, trimmed DNA sequence,
                # a plus sign (standard for fastq file format), and finally the
                # trimmed quality sequence. We print to output file given by
                # the user (or in the case of absent outfile argument, print to
                # "barcode_output.fastq").
                print(tempheader, file = outfastq)
                print(newseq, file = outfastq)
                print('+', file = outfastq)
                print(newqual, file = outfastq)
            else:
                # If no barcode was found in the current DNA sequence, then
                # simply print our header, DNA sequence, a plus sign, and the
                # quality sequence. Print this to the "undetermined.fastq"
                # file:
                print(tempheader, file = outfast2)
                print(tempseq, file = outfast2)
                print('+', file = outfast2)
                print(tempqual, file = outfast2)

# Print a helpful message to indicate that something happened:
print('Done. You can find your trimmed sequences in the file you gave as \
output. (If no output file was given, it was created as \
"barcode_output.fastq".) Any sequences without a barcode are found in the \
file "undetermined.fastq".')