#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###########################################################

Testing if this will be seen on github after I commit?

Title: dna2aa.py
Date: Fri Oct 15 2021
Author: Teodor Alling (https://teodor-a.github.io)

Description:
    This was an assignment for the course 'Programming in Python' as part of
        the Bioinformatics Master Programme at Lund University 2021.
    The program will read DNA sequences from a fasta file and convert the them
        to amino acid sequence, and create a new fasta file with these AA seqs.

List of functions:
    transl()       DNA string as argument, returns corresponding amino acid
                   sequence, with stop codons given as "*". It translates from
                   the start of the string, and therefore if the string length
                   is not a multiple of 3, it omits the final, incomplete
                   codon.

List of modules to import:
    sys        useful functions for using arguments with your program
    re         handy regex functions
    pathlib    I use Path() functions for file and path related housekeeping

Procedure:
    1. Read from DNA file in a fast format.
    2. Convert T -> U
    3. Convert to amino acids using the standard genetic code (we will skip
       the translation to mRNA step this time).
    4. Write to output text file

Usage:
    python dna2aa.py DNA.fna output_file.txt [optional]
    i.e. python dna2aa.py [path to fasta file] [optional path to output file]

Known bugs:
    - Not optimised to also be able to handle fastq format as input. At the
        moment, handles fasta format only.

###########################################################
"""

#%%
#######################################
### Import modules ####################
#######################################

import sys
import re
from pathlib import Path


#%%
#######################################
### Define functions ##################
#######################################

# Make a dictionary with all the codons and their amino acid result. Stop
# codons are given as '*':
codondict = {"UUU" : "F", "CUU" : "L", "AUU" : "I", "GUU" : "V", "UUC" : "F",
"CUC" : "L", "AUC" : "I", "GUC" : "V", "UUA" : "L", "CUA" : "L", "AUA" : "I",
"GUA" : "V", "UUG" : "L", "CUG" : "L", "AUG" : "M", "GUG" : "V", "UCU" : "S",
"CCU" : "P", "ACU" : "T", "GCU" : "A", "UCC" : "S", "CCC" : "P", "ACC" : "T",
"GCC" : "A", "UCA" : "S", "CCA" : "P", "ACA" : "T", "GCA" : "A", "UCG" : "S",
"CCG" : "P", "ACG" : "T", "GCG" : "A", "UAU" : "Y", "CAU" : "H", "AAU" : "N",
"GAU" : "D", "UAC" : "Y", "CAC" : "H", "AAC" : "N", "GAC" : "D", "UAA" : "*",
"CAA" : "Q", "AAA" : "K", "GAA" : "E", "UAG" : "*", "CAG" : "Q", "AAG" : "K",
"GAG" : "E", "UGU" : "C", "CGU" : "R", "AGU" : "S", "GGU" : "G", "UGC" : "C",
"CGC" : "R", "AGC" : "S", "GGC" : "G", "UGA" : "*", "CGA" : "R", "AGA" : "R",
"GGA" : "G", "UGG" : "W", "CGG" : "R", "AGG" : "R", "GGG" : "G"}

# Define a function which will take a DNA sequence as input and return its
# amino acid sequence:
def transl(inputseq):
    # Strip newline characters from input sequence and make into upper case:
    DNAseq = inputseq.strip().upper()
    # Check if there are characters that are not A, T, C, or G in the input:
    if re.search('[^ATCG]', DNAseq) != None:
        # If there are characters that are not DNA nts, print error message:
        print('Please make sure your input is a DNA sequence only,\
without degenerate base indicators and not RNA.')
    else:
        # If there are no non-DNA characters, go ahead and change T to U:
        RNAseq = re.sub('T', 'U', DNAseq)
        # Only go ahead with all complete codons, by subsetting the correct
        # number of nucleotides from the beginning of the string:
        RNAseq = RNAseq[0:len(RNAseq)//3*3]
        # Change sequence to list, with each entry being a codon:
        RNAseq = [RNAseq[nt:nt+3] for nt in range(0, len(RNAseq), 3)]
        # Initialise empty amino acid list:
        AAseq = []
        # Iterate over the list of codons:
        for codon in RNAseq:
            # For each codon, append the corresponding amin acid, as read from
            # the values in the codon dictionary we created before:
            AAseq.append(codondict[codon])
        # Change to string:
        AAseq = ''.join(AAseq)
        # Return the final AA string:
        return(AAseq)


#%%
#######################################
### Arguments and housekeeping ########
#######################################

# Make sure that the input file exists:
if Path(sys.argv[1]).is_file():
    # Takes the argument strings provided and stores them in a variable each:
    infile_fasta = sys.argv[1]
    if len(sys.argv) > 2:
        outfile_txt = sys.argv[2]
    else:
        print('No output file specified by user, using default filename.')
        outfile_txt = 'dna2aa_output.txt'
# If the input file is missing, print error:
else:
    print('\nCould not find input file. Please make sure you are using the \
script as desribed in the documentation.\n \
(python dna2aa.py [path to fasta file] [path to output file])')
    # After this, the program quits:
    print('Exiting. Please try again.\n')
    exit()


#%%
#######################################
### Translate DNA sequences ###########
#######################################

# Initialise varible to store the new line in:
newline = ''

# Open input file for reading from and output file to write to:
with open(infile_fasta, 'r') as infile, open(outfile_txt, 'w') as outfile:
    # Iterate over the lines in the input file:
    for line in infile:
        # Check if the line starts with '>'. These lines are header lines.
        if line.startswith('>'):
            # If it does, check if the string in 'newline' is longer than 0:
            # This is because 'newline' is used for storing DNA sequences in
            # until we encounter a new header. This is to proof the program
            # against input fasta files where the sequence is split over more
            # than one line. This will be easier to understand as you continue
            # reading below.
            if len(newline) != 0:
                # If there is something in 'newline', translate said line to
                # amino acids using transl() which we defined in the beginning
                # of the script.
                newline = transl(newline.strip())
                # Then print this line to the output file:
                print(newline, file = outfile)
                # After it has been printed, we want to empty the variable
                # 'newline' so that we can put a new DNA string in it:
                newline = ''
            # Whether newline is empty or not, we still want to print the
            # current line (which is a header) to the output file:
            print(line.strip(), file = outfile)
        else:
            # If the current line does NOT start with '>', it means it is a
            # line with DNA sequence, and so we append this to our variable
            # 'newline' (which may or may not be empty depending on whether
            # the previous line was also a DNA line as is the case with fasta
            # files with sequence line split over more than one row).
            newline += line.strip()
    # After the last row has been read, there are no more header rows to
    # trigger the initial if clause above, and so we must manually translate
    # the last DNA sequence:
    newline = transl(newline.strip())
    # ... and then print it to the output file:
    print(newline, file = outfile)

# Print a helpful message to indicate that something happened:
print('Done. You can find your amino acid sequences in the file you gave as \
output. (If no output file was given, it was created as "dna2aa_output.txt".)')
