# Mini Bioinfo Tools
This repository contains scripts that have a specific and usually minor but very useful function. Most were created as part of graded exercises in courses completed as part of the masters programme in Bioinformatics at LU. Others have been created as need arises.


## How to use
1. Clone repository
2. Add the path to the repository direcotry to your PATH
3. For specific usage refer to the script file.


## dna2aa.py
This was an assignment completed as part of the course BINP16 at Lund University Oct 2021.

Translate DNA sequences from fasta file, into AA sequences.


## amino_count.py
This was an assignment completed as part of the course BINP16 at Lund University Oct 2021.

Count all amino acids in a file.


## barcode.py
This was an assignment completed as part of the course BINP16 at Lund University Oct 2021.

Trim barcodes from dna sequences in a fastq file.


## FastaParser.py
This was an assignment for the course 'Programming in Python' as part of the Bioinformatics Master Programme at Lund University 2021.

The script reads a text file containing sequence data for mtDNA and Y-chromosome, for a number of individuals. It outputs one fasta file containing the mtDNA sequences, and another output fasta file containing the Y-chromosome sequences.


## BlastParser.py
This is a small script that takes a BLAST output file and restructures it to a (tab-separated) table with five columns, displaying query ID, hit ID, e-value, identity,
and BLAST score of each query. The header looks like so:

`#query target  e-value identity(%) score`

Queries with no hit are still in the table but fields other than the query field are empty.

