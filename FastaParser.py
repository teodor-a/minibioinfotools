#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###########################################################

Title: FastaParser.py
Date: Fri Oct 22 2021
Author: Teodor Alling (https://teodor-a.github.io)

Description:
    This was an assignment for the course 'Programming in Python' as part of
        the Bioinformatics Master Programme at Lund University 2021.
    This program reads a text file containing sequence data for mtDNA and
        Y-chromosome, for a number of individuals. It outputs a fasta file
        containing the mtDNA sequences, and another output fasta file
        containing the Y-chromosome sequences.

List of functions:
    No user defined functions.

List of modules to import:
    sys        useful functions for using arguments with your program
    re         handy regex functions
    pathlib    I use Path() functions for file and path related housekeeping

Procedure:
    1. Open input and output files, and read input line by line.
    2. Depending on what the line contains, saves its contents into a relevant
        variable.
    3. When the program encounters a line with a person's name in it, it will
        print the saved data to the relevant file (mtDNA file or Y chromosome
        file).
    4. Start saving the next person's data in the newly emptied variables.
    5. For the last person, print data manually because there will be no next
        'name' line to trigger the printing.

Usage:
    python FastaParser.py [path to input text file] [path to output fasta file]

Known bugs:
    -

###########################################################
"""


#%%
#######################################
### Import modules ####################
#######################################

import sys
import re
from pathlib import Path



#%%
#######################################
### Arguments and housekeeping ########
#######################################

# Make sure there are at least 3 arguments:
if len(sys.argv) < 3:
    # Otherwise raise exception with helpful message:
    raise Exception('Too few arguments! Must specify input and output files. \
Like so:\n\npython FastaParser.py [path to input text file] [path to output \
fasta file]')
# If number of arguments is at least 3:
else:
    # Make sure that the input file exists:
    if Path(sys.argv[1]).is_file():
        # Takes the argument provided and stores it in a variable:
        inputfile = sys.argv[1]
    # Otherwise:
    else:
        # Raise exception with helpful message:
        raise Exception('\nCould not find input file. Please make sure you \
are using the script as described in the documentation.\n\n(python \
FastaParser.py [path to input text file] [path to output fasta file])\n')
        # After this, the program quits:
        print('Exiting. Please try again.\n')
        exit()  
    # Create two different files based on the core name given by user as
    # argument. They have the same base name but with a suffix each signifying
    # what kind of data the file contains, mtDNA or Y chromosome DNA:
    mtDNAfile = sys.argv[2]+'mtDNA.fasta'
    Ychromfile = sys.argv[2]+'Ychrom.fasta'
    # As long as the output files already exist;
    while Path(mtDNAfile).is_file() or Path(Ychromfile).is_file():
        # If either exists already, print a message:
        print('\nCaution! The provided output file already exists!\n')
        # Ask if user wants to overwrite the files:
        overwrite = input('Overwrite file? [y/n]')
        # If the answer is no;
        if overwrite == 'n':
            # Promt user for new file names:
            newname = input('\nOK, please enter a new output file name: ')
            mtDNAfile = newname.strip()+'mtDNA.fasta'
            Ychromfile = newname.strip()+'Ychrom.fasta'
            # The while loop will then continue.
        # If the answer is yes;
        elif overwrite == 'y':
            print('\nOverwriting output file...\n')
            # Take the original given output name and use it:
            mtDNAfile = sys.argv[2]+'mtDNA.fasta'
            Ychromfile = sys.argv[2]+'Ychrom.fasta'
            # Then stop the while loop, as we don't need to check any more.
            break
        # If an answer other than y or n is given, print message:
        else:
            print('\nMust answer y or n.\n')
            # The while loop will then continue.


#%%

# Make some flags and empty variables, to be used later:
prevlineis_mtDNAheader = False
prevlineis_Ychromheader = False
haemo_status = ''
mtDNA = ''
YDNA = ''
person = ''

# With the input file and both output files open:
with open(inputfile, 'r') as inp, \
open(mtDNAfile, 'w') as mtDNAw, \
open(Ychromfile, 'w') as Ychromw:
    # Iterate over the lines in the input file:
    for line in inp:
        # Strip from newline chars:
        line = line.strip()
        # If line starts with '>';
        if line.startswith('>'):
            # Trim the '>'. This check was added because one of the names in
            # the given input file had a '>' in front... So an additional
            # cleanup I guess.
            line = line [1:]
        # Check if line is made up of the string 'mtDNA'. Using line.upper()
        # to proof program against capitalisation inconsistencies in the input
        # file:
        elif line.upper() == 'MTDNA':
            # If the current line is indeed mtDNA, then flag this so we know
            # in the future:
            prevlineis_mtDNAheader = True
        # Check if line is made up of the string 'Y chromosome'. Using
        # line.upper() to proof program against capitalisation inconsistencies
        # in the input file:
        elif line.upper() == 'Y CHROMOSOME':
            # If the current line is indeed mtDNA, then flag this so we know
            # in the future:
            prevlineis_Ychromheader = True
        # Check if line contains the word 'hemophilia. Using line.upper() to
        # proof program against capitalisation inconsistencies in the input
        # file:
        elif 'HEMOPHILIA' in line.upper():
            # If line contains the hemophilia status of the person, store the
            # line in a variable:
            haemo_status = line
        # Now we check if the flag above is true. If it is, then it means the
        # current line is the actual mitochondrial sequence:
        elif prevlineis_mtDNAheader:
            # Therefore we store the sequence in a variable:
            mtDNA = line
            # Then change the flag back to False:
            prevlineis_mtDNAheader = False
        # Now we check if the flag above is true. If it is, then it means the
        # current line is the actualY chromosome sequence:
        elif prevlineis_Ychromheader:
            # Therefore we store the sequence in a variable:
            YDNA = line
            # Then change the flag back to False:
            prevlineis_Ychromheader = False
        # Check if the current line contains any characters that are not A, T,
        # C, G, hyphen, or question mark. If these are the only characters 
        # present in the line, we can be confident that it is a DNA sequence
        # we are dealing with:
        elif re.search('[^ATCG\-?]', line):
            # If the variable 'person', which is used to store the name of a
            # person in the file in, is not empty;
            if person:
                # we check if the variable haemo_status has been filled:
                if not haemo_status:
                    # If not, fill it with 'unknown':
                    haemo_status = 'unknown'
                # Then, if the variable mtDNA has been filled;
                if mtDNA:
                    # Print the mtDNA data to the relevant file:
                    print('>' + person + ', hemo status: ' + haemo_status,\
                          file = mtDNAw)
                    print(mtDNA + '\n', file = mtDNAw)
                    # Then empty the variable:
                    mtDNA = ''
                # If the variable YDNA has been filled;
                if YDNA:
                    # Print the Y chromosome data to the relevant file:
                    print('>' + person + ', hemo status: ' + haemo_status,\
                          file = Ychromw)
                    print(YDNA + '\n', file = Ychromw)
                    # Then empty the variable:
                    YDNA = ''
            # Finally, whether the variable 'person' has been filled or not,
            # we now replace it with the name which the *current* line
            # consists of:            
            person = line
    # The last person's data will not be printed because there will not be
    # another name line to trigger the printing. So we do it manually:
    if mtDNA:
        print('>' + person + ', hemo status: ' + haemo_status,\
file = mtDNAw)
        print(mtDNA, file = mtDNAw)
    if YDNA:
         print('>' + person + ', hemo status: ' + haemo_status,\
file = Ychromw)
         print(YDNA, file = Ychromw)
   

# Program is finished. Print a message so the user knows something happened:
print('\nDone! Find your data neatly sorted into the files:\n' + mtDNAfile + \
' for mtDNA and ' + Ychromfile + ' for Y chromosome DNA.\n')