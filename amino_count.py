#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
###########################################################

Title: amino_count.py
Date: Sun Oct 17 2021
Author: Teodor Alling (https://teodor-a.github.io)
   
Description:
    This was an assignment for the course 'Programming in Python' as part of
        the Bioinformatics Master Programme at Lund University 2021.
    The program will read amino acid sequences from a fasta file and print the
        total abundances of each amino acid to output file.

List of functions:
    No user defined functions.

List of modules to import:
    sys        useful functions for using arguments with your program
    pathlib    I use Path() functions for file and path related housekeeping

Procedure:   
    1. Read from AA file in a fast format.
    2. Count each amino acid, one variable stores each AA count:
        for each row that is not a header row, tally all characters
    3. If output file was supplied, print final tally to this file. If not,
        print to console.

Usage:
    python amino_count.py amino.faa output_file.txt [optional]
    i.e. python amino_count.py [path to fasta file] [optional path to output file]
    
Known bugs:
    - not optimised to also be able to handle fastq as input

########################################################### 
"""

#%%
#######################################
### Import modules ####################
#######################################

import sys
from pathlib import Path


#%%
#######################################
### Arguments and housekeeping ########
#######################################

# Make sure that the input file exists:
if Path(sys.argv[1]).is_file():
    # Takes the argument strings provided and stores them in a variable each:
    infile_fasta = sys.argv[1]
    if len(sys.argv) > 2:
        outfile_txt = sys.argv[2]
    else:
        print('No output file specified by user, using default filename.')
        outfile_txt = 'amino_count_output.txt'
# If the input file is missing, print error:
else:
    print('\nCould not find input file. Please make sure you are using the \
script as desribed in the documentation.\n \
(python amino_count.py [path to fasta file] [path to output file])')
    # After this, the program quits:
    print('Exiting. Please try again.\n')
    exit()



#%%
#######################################
### Read AA file and count AAs ########
#######################################

# Create dictionary to store the counts of each amino acid in:
aacounts = {'A': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0, 'I': 0,
'K': 0, 'L': 0, 'M': 0, 'N': 0, 'P': 0, 'Q': 0, 'R': 0, 'S': 0, 'T': 0,
'V': 0, 'W': 0, 'Y': 0, 'X': 0}   

# Open input file for reading from:
with open(infile_fasta, 'r') as AAfile:
    # Iterate over the lines in the input file:
    for line in AAfile:
        # Only procees if the current line does not start with '>' i.e. is not
        # a header line:
        if not line.startswith('>'):
            # Strip the line of newline chars, and change all to upper case:s
            line = line.strip().upper()
            # Iterate over the keys in the dictionary we created above:
            for key in aacounts.keys():
                # Add the count of the current AA, to the value stored to that
                # key in the dictionary:
                aacounts[key] += line.count(key)
            # Then iterate over all amino acids in the current line:
            for aa in line:
                # Also count all characters that are not AAs:
                if aa not in aacounts.keys():
                    aacounts['X'] += 1

# Open output file to write to:
with open(outfile_txt, 'w') as outfile:
    for key in aacounts:
        print(key + ' ' + str(aacounts[key]), file = outfile)

# Print a helpful message to indicate that something happened:
print('Done. You can find your amino acid counts in the file you gave as \
output. (If no output file was given, it was created as \
"amino_count_output.txt".)')