#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 14:56:32 2023

@author: Teodor Alling

Purpose: convert BLAST output file to a neat tabular format containing the
         most important information.

Usage: TeodorAlling_blastParser.py -i <input filename> -o <output filename>
       Both -i and -o are optional, as there are default values for both.
       Input file should be a BLAST output file.

How it works:
    1. Read file line by line
    2. If line contains information about query, first write any output if we
       have any complete info (query, target, e-value, identity, and score),
       otherwise just note save (not write) the query name.
       If line specifies no BLAST matches for query, write only query to output.
       If line contains information about target, first write any output if we
       have any, otherwise just save (not write) the target name.
       If line contains information about e-value, save e-value.
       If line contains information about identity, save identity.
       If line contains information about score, save score.
    
"""

###############################################################################
### Modules used ##############################################################
import argparse # For arguments with flags
from os.path import exists # Just for checking if input file exists.
from sys import exit # Just for exiting if file does not exist.
import re # For using regular expressions

###############################################################################
### Parsing arguments #########################################################

# Adds description to program, to be displayed if using the flag -h:
parser = argparse.ArgumentParser(description = "A program to convert a BLAST \
                                 output file to a neat tabular format. Made by\
                                     Teodor Alling for the course BINP28 \
                                         at Lund University.")
# Adds argument -i for use with input filename specification by user:
parser.add_argument('-i', '--Input',
                    help = "Name of file to be parsed. It should be a BLAST \
                        output file. If not specified, filename 'output.txt' \
                            will be used instead.")
# Adds argument -o for use with output filename specification by user:
parser.add_argument('-o', '--Output',
                    help = "Specify name of resulting parsed file. If not \
                        specified, same filename as input, with '.parsed' \
                            added to end, will be used instead.")
# Parses the arguments specified by user:
args = parser.parse_args()

# Check if user specified an input filename:
if args.Input:
    # If so, make the filename specified by user the working input filename:
    inputfilename = args.Input
else:
    # Otherwise, print a friendly message so user knows what is going on:
    print("\nNo filename of file to be parsed has been specified. Using \
filename 'output.txt' instead.\n")
    # ... and specify the default input filename to work with:
    inputfilename = 'output.txt'
# Also, for quality of life we can check that the input file actually exists,
# no matter the filename:
if not exists(inputfilename):
    # If it does not exist, print a helpful message to user:
    print('\nOops. File with the name you specified does not exist. Please \
double check your spelling.\nExiting program.\n')
    # ... and exit the program:
    exit()
    # Note to self: learn more about raising exceptions etc. Seems more applicable.
# Check if user specified an output filename:
if args.Output:
    # If so, make the filename specified by user the working output filename:
    outputfilename = args.Output
else:
    # Otherwise, print a friendly message so user knows what is going on:
    print("\nNo filename for resulting parsed file has been specified. Using \
input filename with '.parsed' added to end instead.\n")
    # ... and specify the default output filename to work with:
    outputfilename = inputfilename + '.parsed'


###############################################################################
# The program #################################################################

# Open file to write to:
outputfile = open(outputfilename, 'w')
# Write the first header row to output file:
outputfile.write('#query\ttarget\te-value\tidentity(%)\tscore\n')

# Keep input file open for reading:
with open (inputfilename) as inputfile:
    # First of all, for quality of life we could check whether input file 
    # is correct format at all:
    if not inputfile.readline().startswith('BLAST'):
        print("\nWarning! Input file does not start with 'BLAST'. Are you \
sure you specified an input file that is a BLAST output file?\nYou should \
double check that the resulting parsed file looks OK.\n")
    # Initialise some variables (one for each column in the output file)
    # and make them empty strings to begin with: 
    query = ''
    targt = ''
    evalu = ''
    ident = ''
    score = ''
    # Loop over all lines in the input BLAST file:
    for line in inputfile:
        # Check if the line contains info about query:
        if 'Query= ' in line:
            # If so, also check if we have a complete row to write to output,
            # i.e. if the variables we created above contain anything (first 
            # iteration they will be empty):
            if query and targt and evalu and ident and score:
                # If the variables are not empty, they must be written to
                # output file, tab separated:
                outputfile.write(query + '\t' + targt + '\t' + evalu + '\t'
                                 + ident + '\t' + score + '\n')
                # Then, they must be emptied so that we don't write them again:
                # (Note: not sure if this is necessary but I'd rather keep it
                # here to make it foolproof.)
                targt = ''
                evalu = ''
                ident = ''
                score = ''
            # We are now ready to find the query:
            query = re.search('Query= ([\w-]+)\s', line).group(1)
            # The above regex explained: group 1 of the resulting object will
            # contain the string inside the parenthesis, i.e. any string 
            # consisting of one or more (+) word characters (\w) and/or
            # dashes (-) found between 'Query= ' and space/tab (\s).
            # This will be the name of the query sequence.
        # Otherwise, check if line tells us that there are no hits found for
        # the query:
        elif 'No hits found' in line:
            # If so, just write a line with no other info that query, to
            # the output file:
            outputfile.write(query + '\t' + '' + '\t' + '' +
                             '\t' + '' + '\t' + '' + '\n')
        # Otherwise, check if line tells us anything about a target match:
        elif line.startswith('>'):
            # Here, we must also write to output if there is information to
            # write. This is checked by checking the contents of the 
            # field variables:
            if query and targt and evalu and ident and score:
                # If indeed they contain info, this means we have a complete
                # query-target information line to write, which we do by:
                outputfile.write(query + '\t' + targt + '\t' + evalu + '\t'
                                 + ident + '\t' + score + '\n')
                # Again, empty variables afterward:
                targt = ''
                evalu = ''
                ident = ''
                score = ''
            # Now we can fill targt variable with the target info:
            targt = re.search('>([\w-]+)', line).group(1)
            # The above regex explained: group 1 of the resulting object will
            # contain the string inside the parenthesis, i.e. any string
            # consisting of one or more (+) word characters (\w) and/or
            # dashes (-) found after the '>'. This will be the name of the
            # target sequence (BLAST match).
        # Check if line tells us anything about score:
        if 'Score = ' in line:
            score = re.search('Score = ([0-9.]+)', line).group(1)
            # The above regex explained: group 1 of the resulting object will
            # contain the string inside the parenthesis, i.e. any string
            # consisting of one or more (+) digits (0-9) and/or full stops (.)
            # found after 'Score = '. This will be the score.
        # Check if line tells us anything about e-value:
        if 'Expect = ' in line:
            evalu = re.search('Expect = (.+),', line).group(1)
            # The above regex explained: group 1 of the resulting object will
            # contain the string inside the parenthesis, i.e. any string
            # consisting of one or more (+) of any character (.) found between
            # 'Expect = ' and ','. This will be the e-value.
        # Check if line tells us anything about identity percentage:
        if 'Identities = ' in line:
            ident = re.search('Identities = [0-9]+/[0-9]+ \(([0-9.]+)%', line).group(1)
            # The above regex explained: group 1 of the resulting object will
            # contain the string inside the parenthesis, i.e. any string
            # consisting of one or more (+) digits (0-9) and/or full stops (.)
            # found between 'Identities = ' followed by any fraction. and '%)'.
            # This will be the identity expressed as a percentage.
            # Yes, this regex might be unnecessarily long, but it is still
            # readable in my opinion, and it makes the program more waterproof.

# When there are no more lines to read, we make sure to close the output file:
outputfile.close()

###############################################################################
# Finish up ###################################################################

print('\nConvertion of file <', inputfilename, '> complete. See file <',
      outputfilename, '> for output.\n', sep = '')